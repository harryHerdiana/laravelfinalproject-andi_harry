<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Siswa extends Model
{
    use HasFactory;
    protected $table = 'siswa';
    protected $primaryKey = 'nis';

    protected $fillable = ['nama','tempat_lahir', 'tanggal_lahir', 'gender', 'agama', 'alamat', 'foto', 'user_id', 'kelas_id'];

    public function class()
    {
        // return $this->belongsTo('App\Models\kelas', 'kelas_id', 'id');
        return $this->belongsTo(kelas::class, 'kelas_id');
    }
}
