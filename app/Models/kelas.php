<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class kelas extends Model
{
    use HasFactory;
    protected $table = 'kelas';

    protected $fillable = ['nama_kelas', 'kapasitas'];

    public function student()
    {
        return $this->hasOne(Siswa::class, 'nis');
    }

    public function users()
    {
        return $this->hasOne(User::class, 'id');
    }
}
