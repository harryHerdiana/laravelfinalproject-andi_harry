<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Guru extends Model
{
    use HasFactory;
    protected $table = 'guru';
    protected $primaryKey = 'nip';
    protected $fillable = ['nama', 'tempat_lahir', 'tanggal_lahir', 'gender', 'agama', 'alamat', 'foto'];
}
