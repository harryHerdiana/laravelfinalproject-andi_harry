<?php

namespace App\Http\Controllers;

use App\Models\Guru;
use App\Models\MataPelajaran;
use Illuminate\Http\Request;


class MataPelajaranController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $matpel = MataPelajaran::all();
        return view('page.matpel.index', ['matpel' => $matpel]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $matpel = MataPelajaran::all();
        $guru = Guru::all();
        return view('page.matpel.create', ['matpel' => $matpel, 'guru' => $guru]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'matpel' => 'required|min:3',
            'deskirpsi' => 'required',
            'guru_nip' => 'required',
        ]);

        $matpel = new MataPelajaran;
        $matpel->matpel = $request->input('matpel');
        $matpel->deskirpsi = $request->input('deskirpsi');
        $matpel->guru_nip = $request->input('guru_nip');
        $matpel->save();

        return redirect('/matpel');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $matpel = MataPelajaran::find($id);
        $guru = Guru::find($matpel->guru_nip);

        return view('page.matpel.detail', ['matpel' => $matpel, 'guru' => $guru]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $matpel = MataPelajaran::find($id);
        $guru = Guru::all();
        return view('page.matpel.edit', ['matpel' => $matpel, 'guru' => $guru]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $matpel = MataPelajaran::find($id);
        $request->validate([
            'matpel' => 'required|min:3',
            'deskirpsi' => 'required',
            'guru_nip' => 'required',
        ]);

        $matpel->matpel = $request->input('matpel');
        $matpel->deskirpsi = $request->input('deskirpsi');
        $matpel->guru_nip = $request->input('guru_nip');
        $matpel->save();
        return redirect('/matpel');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $matpel = MataPelajaran::find($id);
        $matpel->delete();

        return redirect("/matpel");
    }
}
