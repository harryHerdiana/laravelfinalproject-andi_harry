<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Models\kelas;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class KelasController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // $kelas = DB::table('kelas')->get();
        // return view('kelas.show', compact('kelas'));
        $kelas = kelas::all();
        return view('kelas.show', compact('kelas'));

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('kelas.input');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'nama_kelas' => 'required',
            'kapasitas' => 'required|min:1',
        ]);

        $kelas = new kelas;
 
        $kelas->nama_kelas = $request->input('nama_kelas');
        $kelas->kapasitas = $request->input('kapasitas');
 
        $kelas->save();
        return redirect('/kelas');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $kelas = kelas::find($id);
        return view('kelas.edit', compact('kelas'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'nama_kelas' => 'required',
            'kapasitas' => 'required|min:1',
        ]);

        $kelas = kelas::find($id);

        $kelas->nama_kelas = $request->input('nama_kelas');
        $kelas->kapasitas = $request->input('kapasitas');

        $kelas->save();
        return redirect('/kelas');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $kelas = kelas::find($id);
        $kelas->delete();
        return redirect('/kelas');
    }
}
