<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use App\Models\Guru;

class GuruController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $guru = Guru::all();
        return view('page.guru.index', ['guru' => $guru]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $guru = Guru::all();
        return view('page.guru.create', ['guru' => $guru]);
    }

    /*
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'nama' => 'required|min:8',
            'tempat_lahir' => 'required',
            'tanggal_lahir' => 'required',
            'gender' => 'required',
            'agama' => 'required',
            'alamat' => 'required',
            'foto' => 'required|image|mimes:png,jpg,jpeg|max:2048'
        ]);

        $imageName = time() . '.' . $request->foto->extension();
        $request->foto->move(public_path('images'), $imageName);

        $guru = new Guru;
        $guru->nama = $request->input('nama');
        $guru->tempat_lahir = $request->input('tempat_lahir');
        $guru->tanggal_lahir = $request->input('tanggal_lahir');
        $guru->gender = $request->input('gender');
        $guru->agama = $request->input('agama');
        $guru->alamat = $request->input('alamat');
        $guru->foto = $imageName;

        $guru->save();

        return redirect('/guru');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $guru = Guru::find($id);
        return view('page.guru.detail', ['guru' => $guru]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $guru = Guru::where('nip', $id)->first();
        return view('page.guru.edit', ['guru' => $guru]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $guru = Guru::find($id);
        $request->validate([
            'nama' => 'required|min:8',
            'tempat_lahir' => 'required',
            'tanggal_lahir' => 'required',
            'gender' => 'required',
            'agama' => 'required',
            'alamat' => 'required',
            'foto' => 'image|mimes:png,jpg,jpeg|max:2048'
        ]);
        if ($request->has('foto')) {
            $path = 'images/';
            Storage::delete($path, $guru->foto);
            $fotoBaru = time() . '.' . $request->foto->extension();
            $request->foto->move(public_path('images/'), $fotoBaru);
            $guru->foto = $fotoBaru;
        }
        $guru->nama = $request->input('nama');
        $guru->tempat_lahir = $request->input('tempat_lahir');
        $guru->tanggal_lahir = $request->input('tanggal_lahir');
        $guru->gender = $request->input('gender');
        $guru->agama = $request->input('agama');
        $guru->alamat = $request->input('alamat');

        $guru->save();
        return redirect("/guru");
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $guru = Guru::find($id);
        $path = 'images/';
        Storage::delete($path, $guru->foto);
        $guru->delete();

        return redirect("/guru");
    }
}
