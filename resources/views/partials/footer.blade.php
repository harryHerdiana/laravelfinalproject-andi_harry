<section class="info_section layout_padding">
    <div class="container">
        <div class="row">
            <div class="col-md-3">
                <div class="info_menu">
                    <h5>
                        QUICK LINKS
                    </h5>
                    <ul class="navbar-nav  ">
                        <li class="nav-item active">
                            <a class="nav-link" href="/">Beranda <span class="sr-only">(current)</span></a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="/siswa"> Siswa </a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="/guru"> Guru </a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="/matpel"> Mata Pelajaran </a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="/masuk">Masuk</a>
                        </li>
                    </ul>
                </div>
            </div>
            <div class="col-md-3">

            </div>

            <div class="col-md-5 offset-md-1">

            </div>
        </div>
    </div>
</section>

<!-- end info section -->

<!-- footer section -->
<footer class="container-fluid footer_section">
    <p>
        &copy; 2023 All Rights Reserved By
        <a href="https://html.design/">Sanbercode</a>
    </p>

</footer>
