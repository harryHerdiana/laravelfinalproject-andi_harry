<header class="header_section">
    <div class="container-fluid">
        <nav class="navbar navbar-expand-lg custom_nav-container ">
            <a class="navbar-brand" href="/">
                <h3>
                    @yield('title')
                </h3>
            </a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent"
                aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse ml-auto" id="navbarSupportedContent">
                <ul class="navbar-nav  ml-auto">
                    <li class="nav-item {{ Request::path() == '/' ? 'active' : '' }}">
                        <a class="nav-link" href="/">Home <span class="sr-only">(current)</span></a>
                    </li>
                    <li class="nav-item  {{ Str::is('siswa*', Request::path()) ? 'active' : '' }}">
                        <a class="nav-link" href="/siswa"> Siswa </a>
                    </li>
                    <li class="nav-item  {{ Str::is('guru*', Request::path()) ? 'active' : '' }}">
                        <a class="nav-link" href="/guru"> Guru </a>
                    </li>
                    <li class="nav-item  {{ Str::is('matpel*', Request::path()) ? 'active' : '' }}">
                        <a class="nav-link" href="/matpel"> Mata Pelajaran </a>
                    </li>

                    <li class="nav-item  {{ Request::path() == 'login' ? 'active' : '' }}">
                        <a class="nav-link" href="/login">Login</a>
                    </li>
                </ul>
                <form class="form-inline my-2 my-lg-0 ml-0 ml-lg-4 mb-3 mb-lg-0">
                    <button class="btn  my-2 my-sm-0 nav_search-btn" type="submit"></button>
                </form>
            </div>
        </nav>
    </div>
</header>
