@extends('layout.master')
@section('title')
Halaman Input
@endsection

@section('konten')
<form action="/kelas" method="POST">
    @csrf
    <div class="mb-3">
      <label>Nama Kelas</label>
      <input type="text" class="form-control" name="nama_kelas" aria-describedby="emailHelp">
            @error('nama_kelas')
                <div class="alert alert-danger">
                {{ $message }}
                </div>
            @enderror
    </div>
    <div class="mb-3">
        <label>Kapasitas</label>
        <input type="text" class="form-control" name="kapasitas" aria-describedby="emailHelp">
            @error('kapasitas')
            <div class="alert alert-danger">
            {{ $message }}
            </div>
            @enderror
    </div>
    <button type="submit" class="btn btn-primary">Submit</button>
  </form>
@endsection