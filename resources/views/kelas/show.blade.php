@extends('layout.master')
@section('title')
Halaman Tampil
@endsection
@section('konten')
<a href="/kelas/create" class="btn btn-primary">Tambah</a>
<table class="table table-striped table-dark">
    <thead>
      <tr>
        <th scope="col">#</th>
        <th scope="col">Kelas</th>
        <th scope="col">Kapasitas</th>
        <th scope="col">Actions</th>
      </tr>
    </thead>
    <tbody>
        @forelse ($kelas as $key=>$value)
            <tr>
                <td>{{$key + 1}}</th>
                <td>{{$value->nama_kelas}}</td>
                <td>{{$value->kapasitas}}</td>
                <td>                           
                    <form action="/kelas/{{$value->id}}" method="POST">
                        @csrf
                        @method('DELETE')
                        <a href="/kelas/{{$value->id}}/edit" class="btn btn-primary">Edit</a>                                
                        <input type="submit" class="btn btn-danger my-1" value="Delete">
                    </form>
                </td>
            </tr>
        @empty
            <tr colspan="3">
                <td>No data</td>
            </tr>  
        @endforelse              
    </tbody>
</table>
@endsection