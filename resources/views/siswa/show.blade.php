@extends('layout.master')

@section('konten')
{{-- <?php dd($kelas);?> --}}
<h4>{{$siswa->nama}}</h4>
<p>Alamat : {{$siswa->alamat}}</p>
<p>Kelas : {{$kelas->nama_kelas}}</p>

<div class="card text-center">
    <div class="card-header">
      Detail Siswa
    </div>
    <div class="card-body">
      <h5 class="card-title">{{$siswa->nama}}</h5>

        <form method="">
            <div class="row mb-3 text-right">
                <label for="name" class="col-md-4 col-form-label text-md-end">{{ __('Kelas') }}</label>

                <div class="col-md-6 text-left">
                    <input id="name" type="text" class="form-control @error('name') is-invalid @enderror" name="name" value="{{$kelas->nama_kelas}}">

                    @error('name')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                    @enderror
                </div>
            </div>
        </form>
    </div>
    </div>
  </div>
@endsection