@extends('layout.master')
@section('title')
Halaman Tampil
@endsection
@section('konten')
<table class="table table-striped table-dark">
    <thead>
      <tr>
        <th scope="col">#</th>
        <th scope="col">Nama</th>
        <th scope="col">alamat</th>
        <th scope="col">Kelas</th>
        <th scope="col">Actions</th>
      </tr>
    </thead>
    <tbody>
        @forelse ($siswa as $key=>$value)
            <tr>
                <td>{{$key + 1}}</th>
                <td>{{$value->nama}}</td>
                <td>{{$value->alamat}}</td>
                <td>{{$value->class->nama_kelas}}</td>
                <td>                           
                    <form action="/siswa/{{$value->nis}}" method="POST">
                        @csrf
                        @method('DELETE')
                        <a href="/siswa/{{$value->nis}}" class="btn btn-info">Show</a>
                        <a href="/siswa/{{$value->nis}}/edit" class="btn btn-primary">Edit</a>                                
                        <input type="submit" class="btn btn-danger my-1" value="Delete">
                    </form>
                </td>
            </tr>
        @empty
            <tr colspan="3">
                <td>No data</td>
            </tr>  
        @endforelse              
    </tbody>
</table>
@endsection