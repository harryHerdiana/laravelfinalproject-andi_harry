<!DOCTYPE html>
<html>

<head>
    <!-- Basic -->
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <!-- Mobile Metas -->
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
    <!-- Site Metas -->
    <meta name="keywords" content="" />
    <meta name="description" content="" />
    <meta name="author" content="" />

    <title>@yield('title')</title>

    <!-- bootstrap core css -->
    <link rel="stylesheet" type="text/css" href="{{ asset('/template/css/bootstrap.css') }}" />
    <!-- fonts style -->
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:400,700|Poppins:400,600,700&display=swap"
        rel="stylesheet" />
    <!-- Custom styles for this template -->
    <link href="{{ asset('/template/css/style.css') }}" rel="stylesheet" />
    <!-- responsive style -->
    <link href="{{ asset('/template/css/responsive.css') }}" rel="stylesheet" />
</head>

<body>
    <div class="hero_area">
        <!-- header section strats -->
        @include('partials.header')
        <!-- end header section -->
        <!-- slider section -->

        <!-- special section -->

        <!-- end special section -->

        <!-- about section -->
        <section class="about_section layout_padding">

            <div class="container">
                @yield('konten')
            </div>
        </section>

        <!-- end about section -->

        <!-- end course section -->



        <!-- end login section -->

        <!-- info section -->

        @include('partials.footer')

        <!-- footer section -->

        <script type="text/javascript" src="{{ asset('/template/js/jquery-3.4.1.min.js') }}"></script>
        <script type="text/javascript" src="{{ asset('/template/js/bootstrap.js') }}"></script>

        @stack('scripts')
</body>

</html>
