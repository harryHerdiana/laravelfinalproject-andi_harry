@extends('layout.master')

@section('title')
    Tambah Guru
@endsection

@section('konten')
    <form method="POST" action="/guru" enctype="multipart/form-data">
        @csrf
        <div class="form-group">
            <label for="nama">Nama Guru</label>
            <input placeholder="Masukkan nama anda" type="text"
                class="form-control @error('nama')
                is-invalid
            @enderror" name="nama">
        </div>
        @error('nama')
            <div class="alert alert-danger">{{ $message }}</div>
        @enderror
        <div class="form-group">
            <label for="tempat_lahir">Tempat Lahir</label>
            <input placeholder="Masukkan Tempat Lahir anda" type="text"
                class="form-control @error('tempat_lahir')
                is-invalid
            @enderror"
                name="tempat_lahir">
        </div>
        @error('tempat_lahir')
            <div class="alert alert-danger">{{ $message }}</div>
        @enderror
        <div class="form-group">
            <label for="tanggal_lahir">Tanggal Lahir</label>
            <input placeholder="Masukkan Tanggal Lahir anda" type="date"
                class="form-control @error('tanggal_lahir')
                is-invalid
            @enderror"
                name="tanggal_lahir">
        </div>
        @error('tanggal_lahir')
            <div class="alert alert-danger">{{ $message }}</div>
        @enderror
        <div class="form-group">
            <label for="gender">Jenis Kelamin</label>
            <select name="gender" id="gender" class="form-control">
                <option value="">Pilih Jenis Kelamin</option>
                <option value="Laki-laki">Laki-laki</option>
                <option value="Perempuan">Perempuan</option>
            </select>
        </div>
        @error('gender')
            <div class="alert alert-danger">{{ $message }}</div>
        @enderror
        <div class="form-group">
            <label for="agama">Agama</label>
            <select name="agama" id="agama" class="form-control">
                <option value="">Pilih Agama</option>
                <option value="Islam">Islam</option>
                <option value="Katolik">Katolik</option>
                <option value="Protestan">Protestan</option>
                <option value="Hindu">Hindu</option>
                <option value="Budha">Budha</option>
                <option value="Konghucu">Konghucu</option>
            </select>
        </div>
        @error('agama')
            <div class="alert alert-danger">{{ $message }}</div>
        @enderror
        <div class="form-group">
            <label for="alamat">Alamat</label>
            <textarea name="alamat" id="" cols="30" rows="10"
                class="form-control @error('alamat')
            is-invalid
        @enderror"></textarea>
        </div>
        @error('alamat')
            <div class="alert alert-danger">{{ $message }}</div>
        @enderror
        <div class="form-group">
            <label for="foto">Foto</label>
            <input type="file" class="form-control @error('foto')
                is-invalid
            @enderror"
                name="foto">
        </div>
        @error('foto')
            <div class="alert alert-danger">{{ $message }}</div>
        @enderror
        <button type="submit" class="btn btn-primary">Submit</button>
    </form>
@endsection
