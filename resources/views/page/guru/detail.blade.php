@extends('layout.master')

@section('title')
    Detail Pengajar
@endsection

@section('konten')
    <a href="/guru" class="btn btn-primary btn-sm mb-4">Back</a>
    <div class="col-12">
        <div class="card">
            <img class="card-img-top" style="max-width: 350px; margin:auto; margin-top:50px;" src="{{ asset('images/' . $guru->foto) }}" alt="Card image cap">
            <div class="card-body">
                <h5 class="card-title">{{ $guru->nama }}</h5>
                <br />
                <p class="card-text">NIP: <br />{{ $guru->nip }}</p>
                <p class="card-text">Tempat Lahir: <br />{{ $guru->tempat_lahir }}</p>
                <p class="card-text">Tanggal Lahir: <br />{{ \Carbon\Carbon::parse($guru->tanggal_lahir)->locale('id')->isoFormat('dddd, D MMMM Y')}}</p>
                <p class="card-text">Jenis Kelamin: <br />{{ $guru->gender }}</p>
                <p class="card-text">Agama: <br />{{ $guru->agama }}</p>
                <p class="card-text">Alamat: <br />{{ $guru->alamat }}</p>

            </div>
        </div>
    </div>
@endsection
