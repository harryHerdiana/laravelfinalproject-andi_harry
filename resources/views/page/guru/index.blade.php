@extends('layout.master')
@section('title')
    Guru
@endsection

@section('konten')
    <h3 class="card-title">
        Daftar Guru</h3>
    <a href="/guru/create" class="btn btn-primary btn-sm mb-4">Tambah Guru</a>
    <div class="row">
        @forelse ($guru as $item)
            <div class="col-3">
                <div class="card" style="justify-content: end;">
                    <img class="card-img-top" style="max-width: 363px" src="{{ asset('images/' . $item->foto) }}"
                        alt="Card image cap">
                    <div class="card-body">
                        <h5 class="card-title text-center">{{ $item->nama }}</h5>
                        <p class="card-text text-center">{{ $item->nip }}</p>
                        <a href="/guru/{{ $item->nip }}" class="btn btn-primary btn-block">Detail</a>
                        <div class="row mx-auto  my-4">
                            <a href="/guru/{{ $item->nip }}/edit" class="btn btn-warning col-6 ">Edit</a>
                            <div class="col-6 ">
                                <form action="/guru/{{ $item->nip }}" method="POST">
                                    @csrf
                                    @method('delete')
                                    <input type="submit" value="Hapus" class="btn btn-danger btn-block">
                                </form>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        @empty
            <h3>No Movie yet</h3 @endforelse
    </div>
@endsection
