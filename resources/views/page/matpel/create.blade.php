@extends('layout.master')

@section('title')
    Tambah Mata Pelajaran
@endsection

@section('konten')
    <form method="POST" action="/matpel" enctype="multipart/form-data">
        @csrf
        <div class="form-group">
            <label for="matpel">Nama Mata Pelajaran</label>
            <input placeholder="Masukkan nama mata pelajaran" type="text"
                class="form-control @error('matpel')
                is-invalid
            @enderror" name="matpel">
        </div>
        @error('matpel')
            <div class="alert alert-danger">{{ $message }}</div>
        @enderror
        <div class="form-group">
            <label for="deskirpsi">Deskripsi Mata Pelajaran</label>
            <textarea name="deskirpsi" id="" cols="30" rows="10"
                class="form-control @error('deskirpsi')
            is-invalid
        @enderror"></textarea>
        </div>
        @error('deskirpsi')
            <div class="alert alert-danger">{{ $message }}</div>
        @enderror
        <div class="form-group">
            <label for="guru_nip">Nama Pengajar</label>
            <select name="guru_nip" class="form-control">
                <option value="">--pilih nama pengajar---</option>
                @forelse ($guru as $item)
                    <option value="{{ $item->nip }}">{{ $item->nama }}</option>
                @empty
                    <option value="">No Genre options</option>
                @endforelse
            </select>
        </div>
        @error('guru_nip')
            <div class="alert alert-danger">{{ $message }}</div>
        @enderror
       
        <button type="submit" class="btn btn-primary">Simpan</button>
    </form>
@endsection
