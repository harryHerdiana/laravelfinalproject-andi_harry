@extends('layout.master')
@section('title')
    Mata Pelajaran
@endsection

@section('konten')
    <h3 class="card-title">
        Daftar Mata Pelanaran</h3>
    <a href="/matpel/create" class="btn btn-primary btn-sm mb-4">Tambah Mata Pelajaran</a>
    <table class="table">
        <thead>
            <tr>
                <th scope="col">No</th>
                <th scope="col">Nama Mata Pelajaran</th>
                <th scope="col">Aksi</th>
            </tr>
        </thead>
        <tbody>
            @forelse ($matpel as $key=>$item)
                <tr>
                    <th scope="row">{{ $key + 1 }}</th>
                    <td>{{ $item->matpel }}</td>
                    <td>
                        <form action="/matpel/{{ $item->id }}" method="POST">
                            @csrf
                            @method('delete')
                            <a href="/matpel/{{ $item->id }}" class="btn btn-info btn-sm">Detail</a>
                            <a href="/matpel/{{ $item->id }}/edit" class="btn btn-warning btn-sm">Update</a>
                            <input type="submit" value="Delete" class="btn btn-danger btn-sm">
                        </form>
                    </td>
                </tr>
            @empty
                <tr>
                    <td>Data tidak ditemukan</td>
                </tr>
            @endforelse
        </tbody>
    </table>
@endsection
