@extends('layout.master')

@section('title')
    Perbaharui Mata Pelajaran
@endsection

@section('konten')
    <form method="POST" action="/matpel/{{ $matpel->id }}" enctype="multipart/form-data">
        @csrf
        @method('put')
        <div class="form-group">
            <label for="matpel">Nama Mata Pelajaran</label>
            <input value="{{ $matpel->matpel }}" placeholder="Masukkan nama mata pelajaran" type="text"
                class="form-control @error('matpel')
                is-invalid
            @enderror" name="matpel">
        </div>
        @error('matpel')
            <div class="alert alert-danger">{{ $message }}</div>
        @enderror
        <div class="form-group">
            <label for="deskirpsi">Deskripsi Mata Pelajaran</label>
            <textarea name="deskirpsi" id="" cols="30" rows="10"
                class="form-control @error('deskirpsi')
            is-invalid
        @enderror">{{ $matpel->deskirpsi }}</textarea>
        </div>
        @error('deskirpsi')
            <div class="alert alert-danger">{{ $message }}</div>
        @enderror
        <div class="form-group">
            <label for="guru_nip">Nama Pengajar</label>
            <select name="guru_nip" class="form-control">

                @forelse ($guru as $item)
                    @if ($item->nip === $matpel->guru_nip)
                        <option value="{{ $item->nip }}" selected="{{ $item->nip }}">{{ $item->nama }}</option>
                    @else
                        <option value="{{ $item->nip }}">{{ $item->nama }}</option>
                    @endif
                @empty
                    <option value="">No Genre options</option>
                @endforelse
            </select>
        </div>
        @error('guru_nip')
            <div class="alert alert-danger">{{ $message }}</div>
        @enderror

        <button type="submit" class="btn btn-primary">Simpan</button>
    </form>
@endsection
