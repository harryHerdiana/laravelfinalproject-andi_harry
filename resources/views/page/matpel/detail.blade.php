@extends('layout.master')

@section('title')
    {{ $matpel->matpel }}
@endsection

@section('konten')
    <a href="/matpel" class="btn btn-primary btn-sm mb-4">Back</a>
    <div class="col-12">
        <div class="card">
            <div class="card-body">
                <h5 class="card-title">{{ $matpel->matpel }}</h5>
                <br />
                <p class="card-text">Deskripsi: <br />{{ $matpel->deskirpsi }}</p>
                <p class="card-text">Pengajar: <br /></p>
                <div class="row">
                    <div class="col-3 m-auto">
                        <div class="card" style="justify-content: end;">
                            <img class="card-img-top" style="max-width: 303px" src="{{ asset('images/' . $guru->foto) }}"
                                alt="Card image cap">
                            <div class="card-body">
                                <h5 class="card-title text-center">{{ $guru->nama }}</h5>
                                <p class="card-text text-center">{{ $guru->nip }}</p>
                                <a href="/guru/{{ $guru->nip }}" class="btn btn-primary btn-block">Detail Pengajar</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
